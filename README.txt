
Gateway Module  : iTransact
Author          : Sammy Spets <http://drupal.org/user/8038>
                : Synerger Pty Ltd
                : http://synerger.com
Settings        : > Store administration > Configuration > 
                     Payment settings > Edit > Payment gateways
                  
********************************************************************
DESCRIPTION:

The uc_itransact module enables your Ubercart web store to accept payments
through iTransact's payment gateway (www.itransact.com). You need to obtain
an account with them before using this module. iTransact have test accounts
available for developers to use while preparing a site for launch.

********************************************************************
FEATURES THIS MODULE SUPPORTS

This module currently only transacts using the XML method, which
means you'll be charged $5 per month on top of your normal monthly
charges.

Supported features (available through the Ubercart interface):
 * Authorization and capture - Immediately charge the credit card when the
   order is first processed.
 * Pre-authorization - Authorize the payment amount only. The funds are
   locked by iTransact.
 * Post-authorization capture - Capture the amount previously authorized.
   An administrator can alter the amount charged before the capture is
   processed.
 * Void authorization - Void a previously authorized amount. This releases
   the previously locked funds on the card.

The post-authorization capture and void authorization actions are performed
using the Credit Terminal of Ubercart. Access this screen by clicking the
Process card button when viewing the order.

This module does not support cheque (check) payments. Please sponsor
a Drupal developer to add support for this.

********************************************************************
ITRANSACT CONFIGURATION

1. Login to the control panel using your gateway ID and password.

2. Go to the Account settings section.

3. In the Advanced Features section enable API Access. This will cost
   you an extra $5 per month above your other charges.

4. Copy the API Username and API Key into a text file.

5. In the Fraud Control section check the box Require API Access for XML.

6. You can set test mode on in the control panel if you like. However,
   the setting for test mode in Ubercart will override this setting. Be
   careful with this! The author of this module is not responsible for
   any unwanted credit card charges! :)

OPTIONAL (yet recommended)

   Once you've installed the gateway onto the live server add the server's
   IP address to the IP Filter Settings in the Fraud Control section of
   Account Settings.

   Note that this will hamper developer efforts to test so switch this off
   when your developers work on the site.

********************************************************************
UBERCART CONFIGURATION:

1. Go to the payment gateway settings in Ubercart
   > Store administration > Configuration > Payment settings >
       Edit > Payment gateways
  
2. Expand the iTransact fieldset and check the box 'Enable this payment
   for use.'

3. Add the API Username, API Key and Gateway ID to the text fields so
   marked.

4. Check the Enable testing mode check box to put the payment processing
   into test mode. This setting is a master control for test mode. The
   setting in the iTransact Control Panel has no effect!

OPTIONAL

   Use the "Tell iTransact to e-mail the customer" or "Tell iTransact to
   e-mail the merchant" if you would like the iTransact formatted emails.
   Whether these settings override those in the iTransact Control Panel
   hasn't been tested.

********************************************************************
TESTING:

1. MAKE SURE YOU HAVE ENABLED THE TESTING MODE or you'll be charged
   for the transaction! This is done on the Payment gateway settings page
   in Ubercart. This module overrides any settings you make to test mode
   in the iTransact Control Panel. BE AWARE!

2. Use the following:
     Credit card number: 5454 5454 5454 5454
     Expiry date: Anything in the future
     CVV: Anything will be accepted

********************************************************************
THANKS:

Thank you to Ben McCloskey of Engine Industries for sponsoring the
development of this module.

This code was loosely based on the iTransact module for Drupal eCommerce
(http://drupal.org/project/ecommerce). The thank you note in that module
applies to this one too.

"Big thank you to Urbits and the sponsorship of:
   * Richard Bychowski (http://hiranyaloka.com)
   * iTransact"

Last, but not least, thank you to Gordon Heydon. Without some of his
"tight as usual" receipt code this would have taken a bit longer!
Thanks mate!

